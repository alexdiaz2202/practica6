package mx.unam.fciencias.estructuras.lineales.pruebas;

import mx.unam.fciencias.estructuras.lineales.Lista;
import mx.unam.fciencias.estructuras.lineales.ListaDoblementeLigada;

public class PruebaLista {
    public static void main(String[] args) {
        Lista<Integer> lista = new ListaDoblementeLigada<>();
        
        System.out.println("Lista: " + lista);
        lista.agregar(
                Integer.SIZE);
        
        System.out.println("Lista: " + lista);
        lista.agregar(
                Integer.MAX_VALUE);
        
        System.out.println("Lista: " + lista);
        lista.agregar(
                Integer.MIN_VALUE);
        lista.agregar(
                45);
        System.out.println("Lista: " + lista);
        /*
        System.out.println(
            lista.indiceDe(
                Integer.MIN_VALUE));
        
        System.out.println(
            lista.indiceDe(8));
        
        System.out.println(
            lista.indiceDe(
                Integer.MAX_VALUE));
        
        System.out.println(
            lista.get(0));
        System.out.println(
            lista.get(1));
        System.out.println(
            lista.get(2));*/
//        System.out.println(
//            lista.get(3));

//        System.out.println(lista.eliminar(1));
//        System.out.println(lista);
//        System.out.println(lista.eliminar(0));
//        System.out.println(lista);
//        System.out.println(lista.eliminar(1));
//        System.out.println(lista);
//        System.out.println(lista.eliminar(0));
//        System.out.println(lista);
//        System.out.println(lista.eliminar(0));
        
        lista.eliminar(new Integer(Integer.SIZE));
        System.out.println(lista);
        lista.eliminar(new Integer(-2147483648));
        System.out.println(lista);
        lista.eliminar(new Integer(258));
        System.out.println(lista);
    }
}
