package mx.unam.fciencias.estructuras.lineales.pruebas;

import mx.unam.fciencias.estructuras.lineales.Cola;
import mx.unam.fciencias.estructuras.lineales.ColaDinamica;

public class PruebaCola {
    public static void main(String[] args) {
        Cola<Integer> cola =
                new ColaDinamica<>();
        
        cola.insertar(3);
        System.out.println(cola);
        cola.insertar(1);
        System.out.println(cola);
        cola.insertar(9);
        System.out.println(cola);
        cola.insertar(5);
        System.out.println(cola);
        cola.insertar(3);
        System.out.println(cola);
        System.out.println(cola.remover());
        System.out.println(cola.remover());
        System.out.println(cola.remover());
        System.out.println(cola.remover());
        System.out.println(cola.remover());
    }
}
