package mx.unam.fciencias.estructuras.lineales;
import mx.unam.fciencias.estructuras.Nodo;
import mx.unam.fciencias.estructuras.lineales.ListaDoblementeLigada;
public class ColaDinamica<E extends Comparable<E>> implements Cola<E> {
    private Lista<E> elementos;

    public ColaDinamica() {
        this.elementos = new ListaDoblementeLigada<>();
    }
    
    @Override
    public void insertar(E elemento) {
        elementos.agregar(elemento);
    }

    @Override
    public E remover() {
        return elementos.eliminar(0);
    }

    @Override
    public String toString() {
        return elementos.toString();
    }
    
    public E ordenar(){
        int indiceAux = 0;
        Nodo<E> auxiliar = new Nodo<>(elementos.get(0));
        for (int i = 0; i < elementos.tamanio(); i++) {
            if(auxiliar.getElemento().compareTo(elementos.get(i))<0){
                auxiliar.setElemento(elementos.get(i));
                indiceAux = i;
            }
        }
        return elementos.eliminar(indiceAux);
    }
        
}
