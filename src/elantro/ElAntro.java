package elantro;

/**
 *
 * @Autores: Cruz Estrada Valeria Lucero
 *           Diaz Tapia Irving Alexandro
 *           Jimenez Ortiz Brian Federico
 */
public class ElAntro {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*Creamos una instancia de la clase PersonaFormada para poder ocupar sus 
        metodos que generan los datos aleatoriamente*/
        PersonaFormada p1 = new PersonaFormada();
        /*Creamos una instancia de la clase ColaAntro para poder ocupar sus 
        metodos*/
        ColaAntro n = new ColaAntro();
        /* Inicializamos un double con Math.random que va a generar el nuemro de
        personas en la cola de espera del antro*/
        double a =Math.random()*10+10;
        
        /* Ciclo for que usamos para asignar a cada persona sus datos correspondientes*/
        for (int i = 0; i < a; i++) {
            double b = Math.random();//Lo usamos para generar los datos de las
            //personas
            /* Creamos a cada persona*/
            PersonaFormada p = new PersonaFormada(p1.sexoAleatorio(b),
                    p1.dineroAleatorio(),p1.nombre(b)); 
            n.insertar(p);//insertamos en la cola
        }
        
        System.out.println(n.toString()); //Imprimimos la lista 
        System.out.println( "Entran en el orden: "); //Imprimimos la lista ya ordenada
        /* Ciclo for para ordenar a cada persona*/
        for(int i= 0; i<a;i++){
            System.out.println(i+1+".-"+ n.ordenar());
        }
    }
}
