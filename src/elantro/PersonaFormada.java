package elantro; //Importamos el paquete elantro
import java.lang.Math; //Importamos la paqueteria java.lang.Math
/**
 *
 * @Autores: Cruz Estrada Valeria Lucero
 *           Diaz Tapia Irving Alexandro
 *           Jimenez Ortiz Brian Federico 
 */
public class PersonaFormada implements Comparable<PersonaFormada>{ /*
    Creamos una clase publica que implementa elementos comparables y recibe 
    elementos de PersonaFormada
    */
    boolean sexo; //Definimos un booleano para el sexo
    double dinero; //Un double para el dinero
    String nombre; //Un String para guardar el nombre de la persona

    public PersonaFormada() { //Se crea un metodo para definir a la persona formada
        sexo = false; //Se asigna el valor de falso cuando el sexo es femenino 
        dinero = 0.0; //Se asigna una capacida economica
        nombre = "Ana Paula"; //Este string guarda el nombre de la persona formada
    }
    
    
    public PersonaFormada(boolean sexo, double dinero, String nombre) { //Instanciamos los objetos
        this.sexo = sexo; //Se declara sexo
        this.dinero = dinero; //Se declara dinero
        this.nombre = nombre; //Se declara nombre
    }

    

    /**
     * 
     * @param dinero 
     */
    public void setDinero(double dinero) { //Set correspondiente a Dinero
        this.dinero = dinero;
    }

    /**
     * 
     * @param sexo 
     */
    public void setSexo(boolean sexo) { //Set correspondiente a sexo
        this.sexo = sexo;
    }

    /**
     * 
     * @return 
     */
    public boolean isSexo() { //Se crea un metodo publico para el sexo
        return sexo;
    }

    public char getSexo(){ //Este metodo sirve para definir el sexo de la persona formada
        if(isSexo()) //If que en dado caso de cumplirse 
            return 'F';//Regresara "Femenino"
        else //En caso contrario
            return 'M'; //Regresara Masculino 
    }

    public String getNombre() { //get correspondiente a Nombre
        return nombre;//Devuelve el nombre 
    }
    
    /**
     * 
     * @return 
     */
    public double getDinero() { //Get correspondiente a Dinero
        return dinero;//Regresa dinero
    }

    @Override
    public String toString() { //Sobreescribimos el toString
        return nombre+"/"+ getSexo()+"/"+getDinero()+"\n"; 
    /*
     Con ayuda de este toString se imprimira una persona con los datos aqui mostrados
        el ordem sera Nombre, Sexo, Dinero
        */
    }
    public String nombre(double a){ //Este String regresara un nombre aleatoriamente 
        if(a<0.5) //En este caso a variara y determinar si el nombre es de hombre o mujer
            return nombreAleatorioMujer();
        else
            return nombreAleatorioHombre();
    }
    /**
     * 
     * @param a
     * @return 
     */
    public boolean sexoAleatorio(double a){ //Metodo para generar el sexo aleatoriamente 
        return a<0.5;
    }
    /**
     * 
     * @return 
     */
    public double dineroAleatorio(){ //Metodo para generar cantidad economica de la persona formana
        if(Math.random()<0.05) 
            return 600.00;
        else if(Math.random()<0.1)
            return 500.5;
        else if(Math.random()<0.2)
            return 2500.00;
        else if(Math.random()<0.3)
            return 100000.00;
        else if(Math.random()<0.4)
            return 50;
        else if(Math.random()<0.5)
            return 60000.00;
        else if(Math.random()<0.6)
            return 3500.00;
        else if(Math.random()<0.7)
            return 3000.00;
        else if(Math.random()<0.8)
            return 20;
        else if(Math.random()<0.9)
            return 6500.00;
        else 
            return 10000000.00;
    }
    /**
     * 
     * @return 
     */
    public String nombreAleatorioHombre(){ //Metodo para seleciionar los nombres aleatoriamente
        if(Math.random()<0.05)
            return "Jorge";
        else if(Math.random()<0.1)
            return"Saulo";
        else if(Math.random()<0.2)
            return"Angel";
        else if(Math.random()<0.3)
            return"Aaron";
        else if(Math.random()<0.4)
            return"Luis Angel";
        else if(Math.random()<0.5)
            return"Joaquin";
        else if(Math.random()<0.6)
            return"Meridio";
        else if(Math.random()<0.7)
            return"Maximo";
        else if(Math.random()<0.8)
            return"Josue";
        else if(Math.random()<0.9)
            return"Pepe";
        else 
            return "Pedro";
    }
    /**
     * 
     * @return 
     */
    public String nombreAleatorioMujer(){ //Metodo para generar los nombres de mujer aleatoriamente
        if(Math.random()<0.05)
            return "Valeria";
        else if(Math.random()<0.1)
            return"Barbara";
        else if(Math.random()<0.2)
            return"Gabriela";
        else if(Math.random()<0.3)
            return"Maria Jose";
        else if(Math.random()<0.4)
            return"Luz";
        else if(Math.random()<0.5)
            return"Wendy";
        else if(Math.random()<0.6)
            return"Katherine";
        else if(Math.random()<0.7)
            return"Aurora";
        else if(Math.random()<0.8)
            return"Diana";
        else if(Math.random()<0.9)
            return"Antonia";
        else 
            return "Lolita";
    }
    

    @Override
    public int compareTo(PersonaFormada persona){
         //Sobreescrbir el metodo que compara personas 
        if (this.sexo != persona.isSexo()) { //Si es mujer regresa uno, pues tiene prioridad
            return this.sexo ? 1 : -1; //caso contrario regresa -1
        }
        if (this.dinero != persona.dinero && 
                (this.dinero - persona.dinero) < 0) {
            return -1; //Si tiene menos capaciodad economica es -1 
        }
        if (this.dinero != persona.dinero && 
                (this.dinero- persona.dinero) > 0) {
            return 1; // si es al revÃ©s regresa 1
        }
        return 0; //si son iguales regresa cero
    }
}
